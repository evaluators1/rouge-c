package ROUGE;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import libs.ManejoArchivos;
import libs.MyListArgs;
import libs.MySintaxis;

/**
 * ExeROUGE
 * @author Jonathan Rojas Simón <ids_jonathan_rojas@hotmail.com>
 */
public class ExeROUGE
{
    /**
     * General body Perl script command, this body satisfies the following command as example
     * perl ROUGE-1.5.5.pl -e data -n 1 -m -2 4 -u -c 95 -r 1000 -f A -p 0.5 -t 0 -l  
     */
    String CMD = "perl @ROUGE-C@ @REH@ @NGRAM@ @STEM@ @MGL@ @UNSKB@ "
            + "@RSKB@ @LCS@ @CONINT@ @NSAMPL@ @SCOREF@ @ALPHA@ @TOKENS@ "
            + "@OTHER@ @XML@ @SYSTEM@";
    
    boolean gBCalculateLCS;
    int gENGram;
    int gETotalEvals;
    
    ManejoArchivos IO = new ManejoArchivos();

    /**
     * ExeROUGE
     * Constructor that initializes several parameters of ROUGE-1.5.5 system
     * @param lSArgs The file with the command line arguments
     * @param objMyListArgsParam
     */
    public ExeROUGE(MyListArgs objMyListArgsParam, String lSArgs)
    {
        objMyListArgsParam.AddArgsFromFile(lSArgs);

        String lSSintaxis = "-ROUGE:str -REH:str -NGRAM:int -STEM:bool -MGL:int -UNSKB:bool -RSKB:bool -LCS:bool -CONINT:int "
                + "-NSAMPL:int -SCOREF:str -ALPHA:float -TOKENS:int [-OTHER:str] -SYSTEM:int";
        MySintaxis objMySintaxis = new MySintaxis(lSSintaxis, objMyListArgsParam);

        CMD = CMD.replace("@ROUGE-C@", objMyListArgsParam.ValueArgsAsString("-ROUGE", ""));
        CMD = CMD.replace("@REH@", " -e " + objMyListArgsParam.ValueArgsAsString("-REH", ""));
        gENGram = objMyListArgsParam.ValueArgsAsInteger("-NGRAM", 1);
        CMD = CMD.replace("@NGRAM@", " -n " + gENGram);
        CMD = CMD.replace("@STEM@", (objMyListArgsParam.ValueArgsAsBoolean("-STEM", false)) ? " -m " : "");
        CMD = CMD.replace("@MGL@", " -2 " + objMyListArgsParam.ValueArgsAsInteger("-MGL", 0));
        gBCalculateLCS = objMyListArgsParam.ValueArgsAsBoolean("-LCS", false);
        CMD = CMD.replace("@UNSKB@", (objMyListArgsParam.ValueArgsAsBoolean("-UNSKB", false)) ? " -u " : "");
        CMD = CMD.replace("@RSKB@", (objMyListArgsParam.ValueArgsAsBoolean("-RSKB", false)) ? " -U " : "");
        CMD = CMD.replace("@LCS@", (gBCalculateLCS) ? "" : " -x ");
        CMD = CMD.replace("@CONINT@", " -c " + objMyListArgsParam.ValueArgsAsInteger("-CONINT", 95));
        CMD = CMD.replace("@NSAMPL@", " -r " + objMyListArgsParam.ValueArgsAsInteger("-NSAMPL", 1000));
        CMD = CMD.replace("@SCOREF@", " -f " + objMyListArgsParam.ValueArgsAsString("-SCOREF", "A"));
        CMD = CMD.replace("@ALPHA@", " -p " + objMyListArgsParam.ValueArgsAsFloat("-ALPHA", 0.5F));
        CMD = CMD.replace("@TOKENS@", " -t " + objMyListArgsParam.ValueArgsAsInteger("-TOKENS", 0));
        CMD = CMD.replace("@OTHER@", objMyListArgsParam.ValueArgsAsString("-OTHER", ""));
        CMD = CMD.replace("@SYSTEM@", "" + objMyListArgsParam.ValueArgsAsInteger("-SYSTEM", 1));

        gETotalEvals = (gBCalculateLCS) ? gENGram + 2 : gENGram + 1;
    }

    public double[] getEvaluatorsROUGE(String lSConfigXML)
    {
        String lSResultant = executeEvaluation(lSConfigXML);
        double[] lADEvals = new double[gETotalEvals];
        
        for (int i = 0; i < gENGram; i++)
            lADEvals[i] = getFScoreROUGE(String.valueOf(i+1), lSResultant);
        
        lADEvals[gENGram] = getFScoreROUGE("SU4", lSResultant);
        
        if (gBCalculateLCS)
            lADEvals[gENGram + 1] = getFScoreROUGE("L", lSResultant);
        
        return lADEvals;
    }
    
    /**
     * executeEvaluation
     * Method to execute the ROUGE-1.5.5 system
     * @param lSConfigXML 
     * @return A String with
     */
    public String executeEvaluation(String lSConfigXML)
    {
        String lSSalto = "\r\n", lSResultadoComando = "", lSSalida = null;

        CMD = this.CMD.replace("@XML@", "\""+ lSConfigXML + "\"");

        try
        {
            // Ejecucion del Comando para evaluar el archivo
            Process objProceso = Runtime.getRuntime().exec(this.CMD);
            System.out.println(CMD);

            InputStreamReader objEntrada = new InputStreamReader(objProceso.getInputStream());
            BufferedReader objStdInput = new BufferedReader(objEntrada);

            if ((lSSalida = objStdInput.readLine()) != null)
            {
//                System.out.println("Comando ejecutado Correctamente");
                while ((lSSalida = objStdInput.readLine()) != null)
                {
                    lSResultadoComando += lSSalto + lSSalida;
                }
            } else
            {
//                System.out.println("No se ha producido ninguna salida");
            }
        } catch (IOException lEx)
        {
            System.out.println("Excepción: " + lEx.getMessage());
        }
        System.out.println(lSResultadoComando);
        return lSResultadoComando;
    }
    
    /**
     * getFScoreROUGE<br>
     * Method that returns a F-Score from the output perl script command and the system specified (ROUGE-1, ROUGE-2, ..., ROUGE-N, ROUGE-SU4, ROUGE-W, ROUGE-LCS)
     * @param lSSystem A String that relates a system to retrieve (Example: In case of ROUGE 1, specify "1")
     * @param lSOutputCommand Is the output command retrieved from perl execute script
     * @return A F-Score resultant from a system of ROUGE
     */
    public double getFScoreROUGE(String lSSystem, String lSOutputCommand)
    {
//        System.out.println(lSOutputCommand);
        String lSExpresionRegular = "\\d\\sROUGE-" + lSSystem + "\\sAverage_P:\\s([0-9][.][0-9]+)";

        Pattern patron = Pattern.compile(lSExpresionRegular);
        Matcher matcher = patron.matcher(lSOutputCommand);
        matcher.find();
        
        return Double.parseDouble(matcher.group(1).trim());
    }
    
    /**
     * generateFileEvalXML<br>
     * Method that generates the files of evaluation (XML) necesaries to execute the per script command.
     * @param lSNameFile Is the name of evaluation file (Only the name and extension)
     * @param lSReference Is the path and name of reference summary file (Source document)
     * @param lSSummary Is the path and name of candidate summary file (Candidate summary)
     * @return TRUE if the file was generated, FALSE otherwise
     */
    public boolean generateFileEvalXML(String lSNameFile ,String lSReference, String lSSummary, String lSPRef, String lSPSumm)
    {
        String lSContenidoArchivoXML = "", lSSalto = "\r\n";
//        System.out.println(lSReference);
//        System.out.println(lSSummary);

        lSContenidoArchivoXML = "";
        lSContenidoArchivoXML += "<ROUGE_EVAL version=\"1.0\"> " + lSSalto;
        lSContenidoArchivoXML += "<EVAL ID=\"1\">" + lSSalto;
        lSContenidoArchivoXML += "\t <MODEL-ROOT>" + lSSalto;
        lSContenidoArchivoXML += "\t " + lSPSumm + lSSalto;
        lSContenidoArchivoXML += "\t </MODEL-ROOT>" + lSSalto;
        lSContenidoArchivoXML += "\t <PEER-ROOT>" + lSSalto;
        lSContenidoArchivoXML += "\t " + lSPRef + lSSalto;
        lSContenidoArchivoXML += "\t </PEER-ROOT>" + lSSalto;
        lSContenidoArchivoXML += "\t <INPUT-FORMAT TYPE=\"SPL\">" + lSSalto;
        lSContenidoArchivoXML += "\t </INPUT-FORMAT>" + lSSalto;
        lSContenidoArchivoXML += "\t <PEERS>" + lSSalto;
        lSContenidoArchivoXML += "\t\t <P ID=\"1\">" + new File(lSReference).getName() + "</P>" + lSSalto;
        lSContenidoArchivoXML += "\t </PEERS>" + lSSalto;
        lSContenidoArchivoXML += "\t <MODELS>" + lSSalto;
        lSContenidoArchivoXML += "\t\t <M ID=\"b\">" + new File(lSSummary).getName() + "</M>" + lSSalto;
        lSContenidoArchivoXML += "\t </MODELS>" + lSSalto;
        lSContenidoArchivoXML += "</EVAL>" + lSSalto;
        lSContenidoArchivoXML += "</ROUGE_EVAL>" + lSSalto;
        
        return IO.Write_String_File(lSNameFile, lSContenidoArchivoXML);
    }
    
}
