package ROUGE;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import libs.ManejoArchivos;
import libs.MyListArgs;
import libs.MySintaxis;

/**
 * Eval_ROUGE_C
 * Class that evaluates summaries taking into account the source documents (Like the method ROUGE-C).
 * For more details of this method see (He et al., 2008).
 * Moreover, this class allows execute this method as similatity measure or distance measure for other purposes.
 * @author Jonathan Rojas Simón <ids_jonathan_rojas@hotmail.com>
 */
public class Eval_ROUGE_C
{
    /**(-INDIR) gSPTextSumm: Is the path of summaries*/
    String gSPTextSumm;
    /**(-EXT) gSExtension: Is the extension of each file of summaries included in gSPTextSumm*/
    String gSExtension;
    /**(-EXTTD) gSExtTD: Is the extension of each file of reference documents included in gSPTextSrc*/
    String gSExtTD;
    /**(-TODIR) gSPTextSrc: Is the path of reference documents*/
    String gSPTextSrc;
    /**(-OUT) gSOutputFiles: Is the path of output results of each mode; For more details of each mode see the attribute gSType**/
    String gSOutputFiles;
    /**(-READAS) gSType: Determines the option of calculus; EVAL-> As evaluation method, VS -> As similarity measure**/
    String gSType;
    /**(-PARAMS) gSOptionsROUGE: Is the path and name of file that contains the list of options provided by ROUGE*/
    String gSOptionsROUGE;
    /**(-MODE) -> gSMode: Determines the use of this measure as Similarity (SIM), or Distance (DIS). By default is SIM**/
    String gSMode;
    
    String[] gASSummaries;
    String[] gASSourceDocuments;
    
    ManejoArchivos IO = new ManejoArchivos();
    MyListArgs objMyListArgsParam;

    /**
     * Eval_ROUGE_C
     * Constructor that initializes some attributes of this class
     * @param args Is the command line options
     */
    public Eval_ROUGE_C(String[] args)
    {
        objMyListArgsParam = new MyListArgs(args);
        String lSConfigFile = objMyListArgsParam.ValueArgsAsString("-CONFIG", "");//File where specifies the necessary parameters for this component.
        if (!lSConfigFile.equals(""))
            objMyListArgsParam.AddArgsFromFile(lSConfigFile);

        String lSSintaxis = "-INDIR:str -EXT:str -TODIR:str -EXTTD:str -READAS:EVAL:VS -PARAMS:str -OUT:str [-MODE:SIM:DIS]";
        MySintaxis objMySintaxis = new MySintaxis(lSSintaxis, objMyListArgsParam);
        
        this.gSPTextSumm = objMyListArgsParam.ValueArgsAsString("-INDIR", "");
        this.gSExtension = objMyListArgsParam.ValueArgsAsString("-EXT", "");
        this.gSPTextSrc = objMyListArgsParam.ValueArgsAsString("-TODIR", "");
        this.gSExtTD = objMyListArgsParam.ValueArgsAsString("-EXTTD", "");
        this.gSType = objMyListArgsParam.ValueArgsAsString("-READAS", "EVAL");
        this.gSOptionsROUGE = objMyListArgsParam.ValueArgsAsString("-PARAMS", "");
        this.gSOutputFiles = objMyListArgsParam.ValueArgsAsString("-OUT", "");
        this.gSMode = objMyListArgsParam.ValueArgsAsString("-MODE", "SIM");
        
        if (!gSMode.equalsIgnoreCase("SIM") && !gSMode.equalsIgnoreCase("DIS"))
            gSMode = "SIM";
    }
    
    public void run()
    {
        this.gASSummaries = IO.List_Files(this.gSPTextSumm, this.gSExtension);
        this.gASSourceDocuments = IO.List_Files(this.gSPTextSrc, this.gSExtTD);
        
        if (gSType.equalsIgnoreCase("EVAL"))
        {
            exeDIRToCompareEvals(this.gASSummaries, this.gASSourceDocuments);
        } else if (gSType.equalsIgnoreCase("VS"))
        {
            //Evaluation as metric measure
        }
    }
    
    /**
     * exeDIRToCompareEvals
     * @param lSSummaries
     * @param lSSourceDocuments 
     */
    public void exeDIRToCompareEvals(String[] lSSummaries, String[] lSSourceDocuments) 
    {
        HashMap<String, String> objHashMapSummaries = new HashMap<>();
        HashMap<String, String> objHashMapSrcDocs = new HashMap<>();
        ExeROUGE objExeROUGE = new ExeROUGE(objMyListArgsParam, gSOptionsROUGE);
        String lSPath = new File(this.gSOutputFiles).getAbsolutePath().substring(0, new File(this.gSOutputFiles).getAbsolutePath().lastIndexOf(File.separator));
        String lSFileXML = IO.AddToPath(lSPath, "EVAL-ROUGE-C.xml");
        
        Arrays.sort(lSSummaries);

        for (int i = 0; i < lSSummaries.length; i++)
            objHashMapSummaries.put(lSSummaries[i], IO.Reads_Text_File(IO.AddToPath(this.gSPTextSumm, lSSummaries[i])));

        for (int i = 0; i < lSSourceDocuments.length; i++)
            objHashMapSrcDocs.put(lSSourceDocuments[i], IO.Reads_Text_File(IO.AddToPath(this.gSPTextSrc, lSSourceDocuments[i])));
        
        HashMap<String, double[]> objHashMapOutputs = new HashMap<>();
        
        for (Map.Entry<String, String> entry_src : objHashMapSrcDocs.entrySet())
        {
            for (Map.Entry<String, String> entry_summ: objHashMapSummaries.entrySet())
            {
                if (entry_summ.getKey().replace(this.gSExtension, "").toUpperCase().contains(entry_src.getKey().replace(this.gSExtTD, "").toUpperCase()))
                {
                    double[] lADEvals = new double[objExeROUGE.gETotalEvals];
                    if (entry_summ.getValue().equals(""))
                    {
                        if (gSMode.equalsIgnoreCase("DIS"))
                        {
                            for (int i = 0; i < lADEvals.length; i++)
                                lADEvals[i] = 1.0;
                        }
//                        System.out.println(Arrays.toString(lADEvals));
                        objHashMapOutputs.put(entry_summ.getKey(), lADEvals);
                    } else
                    {
                        if (objExeROUGE.generateFileEvalXML(lSFileXML, entry_src.getKey(), entry_summ.getKey(), this.gSPTextSrc, this.gSPTextSumm))
                        {
                            double[] lADValues = objExeROUGE.getEvaluatorsROUGE(IO.AddToPath(lSPath, "EVAL-ROUGE-C.xml"));
//                            System.out.println(Arrays.toString(lADValues));
                            if (gSMode.equalsIgnoreCase("DIS"))
                            {
                                for (int i = 0; i < lADValues.length; i++)
                                    lADValues[i] = 1.0 - lADValues[i];
                            }
                            objHashMapOutputs.put(entry_summ.getKey(), lADValues);
                        }
                    }
                }
            }
        }
        
        new File(IO.AddToPath(lSPath, "EVAL-ROUGE-C.xml")).delete();
        StringBuilder[] objABuilder = new StringBuilder[objExeROUGE.gETotalEvals];
        for (int i = 0; i < objABuilder.length; i++)
            objABuilder[i] = new StringBuilder("");
        for (int i = 0; i < lSSummaries.length; i++)
        {
            System.out.println(lSSummaries[i] + " " + Arrays.toString(objHashMapOutputs.get(lSSummaries[i])));
            double[] lDFinal = objHashMapOutputs.remove(lSSummaries[i]);
            for (int j = 0; j < lDFinal.length; j++)
                objABuilder[j].append(lSSummaries[i].replace(this.gSExtension, "") + "    " + lDFinal[j] + "\r\n");
        }
        
        for (int i = 0; i < objExeROUGE.gENGram; i++)
            IO.Write_String_File(this.gSOutputFiles.replace("@EV@", String.valueOf(i+1)), objABuilder[i].toString());
        
        IO.Write_String_File(this.gSOutputFiles.replace("@EV@", "SU4"), objABuilder[objExeROUGE.gENGram].toString());
        
        if (objExeROUGE.gBCalculateLCS)
            IO.Write_String_File(this.gSOutputFiles.replace("@EV@", "L"), objABuilder[objABuilder.length-1].toString());
        
    }
    
}
