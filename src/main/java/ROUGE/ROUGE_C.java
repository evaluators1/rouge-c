package ROUGE;

/**
 * ROUGE_C
 * This class call a constructor to evaluate summaries without summary models
 * @author Jonathan Rojas Simón <ids_jonathan_rojas@hotmail.com>
 */
public class ROUGE_C
{

    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Eval_ROUGE_C objROUGE_C = new Eval_ROUGE_C(args);
        objROUGE_C.run();
    }
    
}
