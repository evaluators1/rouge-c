#ROUGE perl-script
-ROUGE   ROUGE-1.5.5/ROUGE-1.5.5.pl

#ROUGE_EVAL_HOME to use WordNet (param -e)
-REH     ROUGE-1.5.5/data

#Limit of N-grams (param -n)
-NGRAM   3

#Use of Porter stemming (param -m)
-STEM    true

#Max-gap-length (if < 0 then no gap length limit) (param -2)
-MGL     4

#Include unigram in skip-bigram (param -u)
-UNSKB   true

#Same as -u but also compute regular skip-bigram
-RSKB    true

#Calculate ROUGE-L (param -x)
-LCS     true

#Confidence interval (param -c)
-CONINT  95

#Number of samples (for resampling) (param -r)
-NSAMPL  1000

#Scoring formula [A|B] (param -f)
-SCOREF  A

#Alpha [0-1] (param -p)
-ALPHA   0.5

#Count by token instead of sentence [0|1|2] (param -t)
-TOKENS  0

#Other parameters
*OTHER   -s

#System-ID ROUGE (Last parameter)
-SYSTEM  1