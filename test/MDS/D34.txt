hurricane expert predicts turbulent summer atlantic ocean fiercer storms swirling seas says impossible know storms threaten populated areas 
william gray professor atmospheri science colorado state university said friday expects six atlantic hurricanes year average last 40 years average decade 
atlantic formed relatively hurricanes five last six hurricane seasons 
years brought two five hurricanes except seven hurricanes spotted 1985 
hurricane season officially begins june 1 active period usually begins aug. 1 
gray used wind air pressure patterns make annual hurricane forecasts year since 1984 issue first formal 1988 forecast late may 
issued early outlook friday 10th annual national hurricane conference 
also anticipated average hurricane form 1988 likely intense average hurricanes last six years except 1985 season gray said paper presented conference 
gray said models air pressure winds around equator winds el nino periods eastern pacific waters warmer usual allow predictions hurricane might form 
gray based early outlook presence light easterly winds equator approaching end el nino period 
gray predicted four hurricanes last season three actually occurred 
prediction four 1986 money originally predicted eight hurricanes adjusting figure seven eventual correct number 1985 
1984 five hurricanes gray predicted seven 
drought west africa responsible drop atlantic hurricanes 1970s 1980s gray said 
said drought robbed storm systems moisture needed start escalation hurricanes 
pattern eventually change said impossible say shift occur 
forecasters emergency management officials conference stressed coastal populations increased rapidly lull hurricanes surge storms levels 1950s 1960s could bring unprecedented damage said 
study found u.s. death toll major hurricane could far worse previously predicted head national hurricane center said tuesday mark opening 1988 atlantic storm season 
recently completed hurricane coastal flood models atlantic gulf coasts show many people earlier thought must evacuated certain conditions said center director bob sheets 
forty-three million people live 175 coastal counties maine texas evacuating crowded urban areas barrier islands virtually impossible sheets said 
enough shelters handle added load serious concerns new glass-windowed high-rises would fare lashed hurricane-force winds evacuations said 
hurricane struck houston glass flying everywhere said sheets 
refused make predictions 1988 hurricane season saying one convinced forecasters reliably predict number severity storms 
first tropical depression year formed remained stationary tuesday night south western tip cuba 200 miles southwest havana 
system contained winds 30 mph expected strengthen 
one two satellites used keep track hurricanes could fail year forecasters said 
quite possible year could lose goes-west anytime hurricane specialist bob case said geo-stationary earth-orbiting environmental satellite records atmospheric conditions pacific ocean part western hemisphere 
satellite stop working goes-east would directed move equatorial orbit brazil spot south texas gulf coast would provide view western hemisphere limited vision hurricane-spawning eastern atlantic said 
previous goes-east expired 1984 temporarily deprived meteorologists atlantic atmospheric photographs 
typical atlantic hurricane season june 1 nov. 30 10 named tropical storms rain maximum sustained wind exceeding 39 mph six become hurricanes drenching rain wind 74 mph case said 
1987 three hurricanes four tropical storms atlantic 
hurricane emily slammed dominican republic sept. 22 causing three deaths wind gusts 110 mph 
recovered enough punch belt bermuda 116 mph wind three days later 
emily fastest moving hurricane known century case said 
hurricane arlene meandered atlantic mid-august top wind 75 mph hurricane floyd hit key west 75 mph wind fizzled everglades miami mid-october 
names given atlantic tropical storms could grow hurricanes season alberto beryl chris debby ernesto florence gilbert helene isaac joan keith leslie michael nadine oscar patty rafael sandy tony valerie william 
dealing first major hurricane director national hurricane center bob sheets skipping beat tuesday multitude reporters fired questions 
sheets acting director 1987 atlantic hurricane season named director march spent themorning sixth floor hurricane headquarters beside large monitor showing destructive hurricane gilbert sprawled across much western caribbean 
schedule interviews timed minute 
sheets rosy-cheeked good-natured much flinch seat 
noted 've flown 200 hurricanes really think difficult 
transition former director neil frank widely known distinctive style flattop haircut sheets dark blond hair carefully coiffed wearing bright pink shirt gray wool-blend jacket smooth one 
knows job well calm personality vivian jorge center budget analyst said sheets 
days active tropical weather ms. jorge steps coordinate media interviews 
interest gilbert high since started packing hurricane-force winds weekend tuesday afternoon became category 5 storm winds 160 mph central pressure 26.66 inches 
category 5 hurricane saffir-simpson scale strength winds excess 155 mph pressure less 27.17 inches potential causing catastrophic damage 
would considered `great hurricane said sheets 
certainly top 10 percent far intensity size destructive potential 
gilbert compared hurricane 1935 slammed florida keys killed 600 people hurricane camille devastated mississippi coast 1969 killing 256 
category 5 storms made landfall century 
last major hurricane make landfall elena 1985 along mississippi alabama florida panhandle 
major television networks local stations newspapers media hand monitor hurricane could reach gulf mexico thursday said sheets 
anybody guess go said 
brand new ball game far continental united states concerned said sheets 
center 33-member crew including seven hurricane forecasters specialists keeping close watch hurricane large open room blue monitors flickering color non-color graphics 
sheets might appear spend day tiny microphones snaking back tv interviews fielding questions also writes hurricane advisories issued every three hours makes hurricane forecasts 
dealing major hurricane much different dealing smaller ones tropical storms sheets said adding gilbert particularly well-behaved 
think going feel lot confident 're weaker systems said 
winds hurricane gilbert clocked 175 miles per hour u.s. weather officials called gilbert intense hurricane ever recorded western hemisphere 
mark zimmer meterologist national hurricane center reported air force reconnaissance plane measured barometric pressure gilbert center 26.13 inches 5:52 p.m. edt tuesday 
gilbert barreling toward mexico yucatan peninsula 
lowest pressure ever measured western hemisphere zimmer said 
previous record low pressure 26.35 inches set 1935 labor day hurricane struck florida keys winds 150 mph killing 408 people said 
tropical storm force winds extending 250 miles north 200 miles south hurricane center gilbert also one largest 
circumference hurricane changes often course records kept overall size said center meterologist jesse moore 
hurricane debby barely crossed 74 mph threshold hurricane strength striking mexico last month probably half size moore said 
gilbert one three category 5 storms hemisphere since weather officials began keeping detailed records 
others 1935 labor day hurricane hurricane camille bulldozed mississippi coast 172 mph winds 28-foot wave 1969 leaving 1.4 billion damage 256 dead 
1900 hurricane responsible worst natural disaster u.s. history however 
storm hit galveston texas sept. 8 killed 6,000 people 
category 5 storms winds greater 155 mph barometric pressure less 27.17 inches storm surge higher 18 feet 
storm surge great dome water follows eye hurricane across coastlines bulldozing everything path accounts nine 10 hurricane fatalities 
camille storm surge 25 feet high hurricane center forecasting surge 8-12 feet gilbert zimmer said 
damage worst-case hurricanes catastrophic _shrubs trees blown street signs gone roofs windows blown away shattered mobile homes destroyed 
moisture heat drives hurricane zimmer said 
engine tall chimney warm moist air center 
atmospheric conditions general allow warm chimney build high levels 10-12 miles high severe hurricane 
category 4 storms cause extreme damage winds 131 155 mph surge 13-18 feet pressure 27.17 27.90 
weakest hurricanes category 1 cause minimal damage winds 74 95 mph 4-5 foot surge pressure 28.94 
hurricane gilbert record-breaking fury sends caribbean islanders scrambling cover national hurricane center director bob sheets remained calm helm 
barometric pressure storm center plummeted 26.13 inches 5:58 p.m. edt tuesday night making gilbert intense hurricane ever recorded western hemisphere terms barometric pressure sheets handled mob reporters ease 
've flown 200 hurricanes really think difficult said 
schedule interviews timed minute sheets spent tuesday sixth floor hurricane headquarters beside large monitor showing destructive gilbert full color sprawled across much western caribbean 
rosy-cheeked good-natured sheets named acting director 1987 atlantic hurricane season became director march 
transition former director neil frank widely known distinctive style flat-top haircut sheets dark blond hair carefully coiffed wearing bright pink shirt gray wool-blend jacket smooth 
knows job well calm personality vivian jorge center budget analyst said sheets 
days especially active tropical weather ms. jorge steps coordinate media interviews 
interest gilbert high since started packing hurricane-force winds last weekend tuesday night became category 5 storm winds 175 mph 
category 5 hurricane saffir-simpson scale strength potential causing catastrophic damage winds excess 155 mph pressure 27.17 inches 
gilbert compared labor day hurricane 1935 slammed florida keys killed 408 people hurricane camille devastated mississippi coast 1969 killing 256 category 5 storms hit land century 
last major hurricane make landfall elena 1985 along mississippi alabama florida panhandle 
major television networks local stations print media hand monitor hurricane 
center 33-member crew including seven hurricane forecasters specialists keeping close watch hurricane large open roomful blue monitors flickering graphics 
sheets might appear spend day tiny microphones snaking back fielding media questions also writes hurricane advisories issued every three hours makes hurricane forecasts 
dealing major hurricane much different dealing smaller ones tropical storms sheets said adding gilbert particularly well-behaved 
think going said 
feel lot confident 're weaker systems 
previous experience hurricanes served well keeping pressure sometimes 18-hour days 
've done 25 years one stage 're prepared feel lot pressure said sheets eating lunch 
sheets said remains touch predecessor frank get special advice friend dealing high-visibility post hurricane center 
frank television forecaster texas station khou-tv getting geared houston said sheets smile 
hurricane joan 80 mph winds churned across open caribbean today unusual southern path forecasters puzzling potential strength possible landfall 
report storm left 50 people dead missing town colombia day 
joan one kind said jim gross meteorologist national hurricane center 
see many hurricanes take course hug coast 
colombia official state bolivar northern part country said storm triggered flooding town carmen de bolivar 360 miles north capital bogota 
water roared three gullies town storm passed monday victor leon mendoza state government administrator told colombian radio chain rcn 
preliminary reports mayor office indicate 50 persons dead missing town said 
noon edt today joan center near latitude 11.1 north longitude 76.3 west 70 miles west colombian coast 380 miles east island san andres coast nicaragua 
hurricane moving west 10 mph expected continue motion today 
panama issued hurricane watch north coast colombia issued hurricane watch san andres part colombia even though located close nicaragua 
joan atlantic season fifth hurricane expected bring 4 8 inches rain along path 
tropical storm 45 mph winds joan slowly built strength skimmed colombia coast baffling forecasters predicted weaken 
reached hurricane force monday night center began become better defined sign system could intensify open water western caribbean forecasters said 
present path would least 48 hours main part hurricane reached central america gross said late monday 
gross said strong high-pressure system northern caribbean keeping joan course 
normally prevailing winds upper-level atmospheric conditions push tropical storms northward move atlantic said gross 
also said hurricanes often sapped strength far southern caribbean due trade winds push colder deep-ocean water surface said gross 
joan became 10th named storm 1988 atlantic hurricane season top winds passed 39 mph oct. 11 hurricane strength 74 mph 
gilbert devastating hurricane recent years left 300 people dead caused billions dollars damage tore jamaica cayman islands mexico yucatan peninsula last month 
six-month hurricane season ends nov. 30 
forecasters preparing thursday opening atlantic hurricane season wish could predict arrival new technological help say may crucial ever-growing coastal populations 
air force agreed fly hurricane reconnaissance flights two years made clear plans phase missions 
one satellite available tracking hurricanes 
nothing right lean says ken mckinnon spokesman u.s. rep. tom lewis north palm beach fla. introduced bill congress keep hurricane hunters flying least another five years 've got one satellite 're telling us 'll job 
blinks track weather ''the air force want involved 
last years examined need manned weather reconnaissance feel real compelling military reason said spokesman lt. col. darrell hayes 
're disputing hurricane center weather service need data 
're saying may appropriate agencies provide information said adding service approached national oceanic atmospheric administration taking flights 
besides flights forecasters depend radar satellite data 
single working weather satellite intended alone 
second satellite failed replacement failed craft blown mishap launch pad forcing forecasters make 
new satellites horizon says bob sheets director national hurricane center 
've due long time expected late 1990 
major concern us sheets said 
forecasters also worried shift pattern hurricane activity recent years 
since 1985 sheets said seem hurricanes 're likely hit united states 
may upswing said possibly back pattern '40s '50s '60s tremendous number landfall hurricanes 
max mayfield hurricane specialist national weather service miami said experts known enough yet hurricanes tell peak activity return 50s 60s 
see past antilles atlantic toward hawaii west said forecaster hal gerrish 
'd like able see way africa atlantic hurricanes develop said 
need improved tracking systems important people moving coastal locations likely affected storms 
spoke 5,000 people west coast florida sheets said 
ninety-plus percent midwest northeast come florida 
really little concept hurricane 
average atlantic hurricane season stretches june end november six tropical storms grow hurricanes heavy rains winds 74 mph 
donna 1960 struck florida keys marathon raked across naples fort myers weakening inland 
last season 505 people died atlantic hurricanes including gilbert joan 
gilbert killed 300 people heavy damage mexico jamaica haiti dominican republic blasted across western caribbean part gulf mexico including florida keys florida straits cuba 
joan hovered coast central america days howling top winds 135 mph 
storm caused mudslides floods damage nicaragua costa rica colombia panama 
officials warned residents u.s. virgin islands puerto rico nearby islands bolt everything loose stock food water wednesday hurricane dean rumbled eastern caribbean 
dean upgraded tropical storm second hurricane atlantic season wednesday nightfall national weather center puerto rico reported hurricane winds strengthened 80 mph 
hurricane warnings posted leeward islands antigua u.s. virgin islands puerto rico said forecasters national hurricane center near miami 
midnight edt forecasters reported center hurricane latitude 18.5 north longitude 61.3 west moving westerly 15 mph latest position 310 miles east puerto rico 65 miles northeast barbuda 240 miles east st. thomas 
storm moving warm tropical waters 15 mph slightly 20 mph sustained much day 
forecasters said strengthening possible next 24 hours 
advisory issued national weather service san juan u.s. virgin islands puerto rico advised residents secure loose objects move indoors board tape windows stock emergency supplies drinking water food needs refrigeration batteries 
dangerous storm taken lightly even though minimal hurricane said 
take chances 
could lead injuries even death 
san juan city 1.1 million people shoppers formed long lines supermarkets workers boarded windows governor mansion stores tourist district old san juan 
hurricane advisory said aircraft reports indicated dean center moving westward making temporary jog northwest 
increases threat northern leeward islands said 
forecasters said storm expected first hit land barbuda easternmost leeward island move northwest toward u.s. virgin islands puerto rico next 24 36 hours 
government radio twin island state antigua barbuda said dean expected hit barbuda wednesday night warned residents bar windows latch loose objects stock water 
barbuda flat 62-square-mile coral island population 1,200 little industry tourism 
hurricane warning guadeloupe dropped hurricane watch dominica martinique 
coral gables fla. hurricane specialist jim gross called dean small hurricane hurricane-force winds confined within 25 miles center 
rainfall 4 8 inches tides 2 4 feet normal said possible storm path 
authorities broadcast similar warnings several nearby islands 
dean grew fifth tropical depression season monday morning named storm monday night 
three storms formed since season began june 1 
tropical storm allison caused widespread flooding texas louisiana june 
barry churned open atlantic last month dissipating 
chantal became hurricane monday downgraded tropical depression tuesday night hitting land texas 
supercomputers satellites expertise several hurricane forecasters predicted destructive path hurricane hugo would follow giving people plenty time flee south carolina coast 
hurricane tracking remains uncertain science 
forecasters national hurricane center used computer models track hugo path charleston s.c 
world knowledge meteorological conditions forecasting changes conditions embodied models said thomas pyke head national oceanic atmospheric administration satellite service 
pinpointing exact point hugo landfall difficult forecasters said friday landfall predicted time formevacuation 
overall think tracking models gave us good idea hugo would officials south carolina could act timely manner said research meteorologist colin mcadie 
real forecasting problem hugo predicting intensity storm upgraded category 4 hurricane hours slammed charleston 
difficult predict changes intensity reliable computer models mcadie said 
really need improve forecasting ability strength 
hurricane specialists surprised last-minute increase wind speed reported air force reconnaissance 
hurricane specialist gil clark tracked hurricanes 35 years said couple decades ago forecasting tools reports aircraft ships 
radar satellites needless say forecasts less accurate clark said 
late 1960s weather service began using satellites obtain global weather picture 
information satellite used improve accuracy large-scale models television viewers see every night 
using information satellite supercomputers national meteorological center suitland md. send information hurricane center tracking model constantly changes account current weather conditions position hurricane 
determine track storm forecasters analyze upercomputer predictions satellite data history similar storms current path hurricane 
make educated guess landfall 
meteorology professor kerry a. emmanuel massachussetts institute technology criticizes current forecasting system 
congress american people suffering collective delusion data problems solved satellites true emmanuel said 
satellites give pretty picture said enough information wind temperatures affect hurricane path 
information actually used predict hurricanes comes flying airplanes hurricane good job emmanuel said 
forecasters say accuracy satellite pictures improving every year long-range forecasting become precise 
remember models used guidance products pyke said ultimately job forecaster predict storm path 
people caught hurricane hugo last year might disagree forecasters say deadly storm may positive side effect got public attention 
one forecaster says hurricane seasons may getting worse 
hugo caused unprecedented 10 billion damage killed 28 people lesser antilles islands additional 29 south carolina 
would much deadly hit almost anywhere else says bob sheets director national hurricane center 
advent atlantic hurricane season runs june 1 nov. 30 sheets hurricane experts using hugo example get attention complacent coastal residents 've never experienced fury 
'll take advantage fact hugo last year raise people awareness said sheets 
consequences prepared great 
early warnings hugo last september allowed 350,000 people evacuate safely south carolina worst hurricane struck francis marion national forest charleston myrtle beach sheets said 
heavily damaged fishing village mcclellanville several small rural communities population sparse 
hugo struck major coastal population center destruction would greater americans ever seen according computer simulations known slosh models sea lake overland surges hurricanes 
slosh forecasters predict height storm surge mass water piled storm hurricane destructive component anywhere along u.s. coast punching storm speed size intensity sheets said 
population density south carolina lot different florida coast new jersey galveston texas sheets said 
compare situation miami-through-palm beach area dade broward palm beach counties would destroyed 
hugo worst hurricane strike southeastern u.s. coast since betsy hit florida keys 1965 killing 74 went missisippi louisiana 
since population areas south florida ballooned residents never directly experienced hurricane 
according one nation leading hurricane experts hugo may first new era killer storms 
one knows sure odds florida east coast going get william gray professor atmospheric science colorado state university told national conference weather experts month 
gray came conclusion usually accurate predictions hurricane activity mark last year 
figured 1989 season would relatively mild four hurricanes instead seven hurricanes four tropical storms killed total 84 people 
blew pretty bad sheets said 
looked rainfall africa found amazing correlation rainfall hurricane activity florida 
gray realized 30-year drought africa sahel region corresponds almost exactly years major hurricanes struck southeastern coast 
whether one causes uncertain 
may reflect larger-scale events sahel getting near-normal rainfall sheets said 
gray plans release predictions 1990 june 5 
tropical storms recorded atlantic every month except april rare outside june 1 nov. 30 season 
last week preseason tropical depression brought heavy rain cuba 
tropical depression becomes tropical storm given name sustained winds reach 39 mph becomes hurricane winds reach 74 mph 
names atlantic hurricanes tropical storms year arthur bertha cesar diana edouard fran gustav hortense isidore josephine klaus lili marco nana omar paloma rene sally teddy vicky wilfred 
1990 atlantic hurricane season begins today amid dire warnings killer storms east gulf coasts last two years may harbingers new era destructive storms 
hurricane season runs nov. 30 ushered tropical depression last week caribbean brought heavy rain cuba south florida intensify hurricane 
many coastal communities swelling populations ill-prepared handle hurricane emergency said robert sheets director national hurricane center 
recent national oceanic atmospheric administration report predicts number people seaside counties maine texas grow 60 percent counting 1960 2010 
states florida texas experience near 200 percent growth period report said 
're looking possibility greater destruction greater loss life sheets said thursday 
ca stop hurricanes 
thing work better preparedness emergency planning 
aftermath hurricane gilbert 1988 hugo last year taught officials improvements needed better evacuate protect estimated 45 million coastal residents maine texas sheets said 
long-term trends indication come frequent stronger hurricanes said sheets 
gilbert killed 300 people caused heavy damage lesser antilles mexico 
hugo killed 28 people eastern caribbean 29 south carolina caused record 10 billion damage 
sheets said predictions increased hurricane activity based studies past decades atmospheric low-pressure waves increased rainfall trends west africa near breeding waters hurricanes 
hurricane activity started dropping drought conditions began early 1960s africa sahel region said 
1940 1969 united states hit 22 hurricanes minimum winds 110 mph 1970 1989 eight storms including hugo sheets said 
hope catch year decade '40s said 
five categories hurricanes ranging category 1 top sustained winds 74 mph 95 mph category 5 top winds greater 155 mph 
gilbert hugo reached category 5 according meteorologist barry fatchwell national hurricane center 
sheets praised south carolina officials response hugo said leaders proverbial act together lives may saved 
also said hugo showed inadequacies emergency broadcast system communities used emergency shelters ill-suited withstand powerful hurricane 
caribbean officials worked improve communications systems links cut gilbert 
1990 atlantic hurricane season storms¿ usual forecasters predict hint frequent forceful hurricanes coming years 
year batch include devastating storms years past least one forecaster thinks 1991 calmer year 
season ends today 
dr. william gray professor atmospheric science colorado state university thinks dry spell west africa past two decades explained reduction storms along eastern seaboard last 20 years 
1988 1989 near normal rainfall sahel semi-arid land southern fringe sahara desert 
meteorological experts think rainfall signaled end 20-year drought may spawned gilbert 1988 hugo 1989 
gray thursday predicted below-average hurricane season 1991 
based forecast several factors including anticipated below-average rainfall sahel 
gray forecasters agree general outlook 1990s early years next century intense hurricanes last two decades 
've real long period hurricane inactivity '70s '80s 1985 think 're coming said bob sheets director national hurricane center coral gables 
perhaps gilberts hugos signaling said recalling hurricane gilbert wracked caribbean hurricane hugo devastated south carolina two destructive atlantic hurricanes ever 
hurricanes recorded atlantic every month except april rare outside hurricane season june 1 nov. 30 
year season 14 named storms eight hurricanes 
average number storms nine six becoming hurricanes 
usually couple storms strike u.s 
many storms spun harmlessly atlantic several caused share destruction 
deadly storm hurricane diana swept mexico early august resulting flash floods mudslides killed 96 people caused extensive damage roads property agriculture weather service reported 
weather disturbance becomes tropical storm given name sustained winds reach 39 mph 
storms become hurricanes winds reach 74 mph 
hurricane chantal first season aimed texas louisiana coasts monday day storm winds capsized oil drilling work vessel trapped many 10 crew members inside 
three crewmen picked nearby fishing vessel fourth plucked water coast guard helicopter 
coast guard also said unconfirmed reports two others picked another boat 
rescue divers however unable search overturned service vessel survivors increasingly strong winds high waves 
hope least crew would airtight cabins enough oxygen survive help reached 
coast guard petty officer bob morehead said search called late afternoon winds excess 60 m.p.h waves 12 feet building 
shortly thereafter national hurricane center miami announced chantal blowing 74 m.p.h. strong enough move lowest hurricane classification winds later reported 75 m.p.h 
72-foot service vessel leased chevron corp. avis bourg co. capsized 25 miles south morgan city la self-propelled vessel legs extended ocean floor 
boat ordered port impending storm 
late monday afternoon chantal 200 miles south-southeast galveston tex. traveling northwest 10 m.p.h officials national hurricane service miami said storm expected reach land time afternoon evening 
best estimate monday center hurricane would hit western louisiana upper texas coast 
hurricane warnings issued freeport tex. morgan city 
tropical storm warnings issued far south port o'connor tex. far east mobile ala. bob ebaugh weather specialist hurricane center said 10 15 inches rain expected southern mississippi louisiana east texas 
meanwhile coastal residents began long ritual along gulf coast hurricane season begins late spring runs early fall 
grocery stores began reporting steady demand hurricane staples bottled water batteries canned goods tape windows 
one particular concern houston possibility major flooding city wet eastern side hurricane 
event houston area would extensive flooding said bill evans flood watch director harris county flood control district 
besides heavy rains wind hurricane expected cause tides 5 7 feet normal 
chantal third named storm since beginning hurricane season june 1 tropical storm allison dumped huge amounts rain texas louisiana june tropical storm barry dissipated atlantic without reaching land 
names different hurricanes powerful punches hugo gilbert may prowling atlantic caribbean gulf mexico future 
probability intense hurricanes atlantic region greater next decade two 1970s '80s says meteorologist william m. gray colorado state university analyzes hurricane patterns 
gray predicts possible return ferocious hurricanes '50s '60s apparent break periodic west african drought 
rainfall sahel typically associated intense hurricane activity average 1988 first time since 1969 says 
second rainy summer year indicates end drought 
intense hurricanes gray explains usually form low latitudes tropical disturbances moving westward africa 
well-watered conditions '50s '60s produced 31 severe kind categories 4 5 17-year period 1950 1967 
hurricanes classified saffir-simpson scale fiercest 5 catastrophic storm 
atmospheric pressure center drops drastically wind speed exceeds 155 m.p.h 
drier 17-year period 1970 1987 13 severe storms 
'88 '89 seasons june november five 
last year gilbert left wide swath devastation across jamaica mexican yucatan mightiest hurricane record western hemisphere 
atmospheric pressure dropped 885 millibars wind speed reached 200 m.p.h 
september hugo ripped virgin islands puerto rico clobbering south carolina sustained winds 150 m.p.h atmospheric pressure 918 millibars 27.1 inches 
officially 4 saffir-simpson scale may borderline 5 says meteorologist mark zimmer national hurricane center miami 
strongest recorded storm earth zimmer says 1979 typhoon tip western pacific low pressure 870 millibars 
outside atlantic area eastern pacific hurricanes called typhoons cyclones 
fortunately atlantic-region hurricanes develop worst potential 
century two 5 hurricanes struck united states full force 1935 labor day storm ravaged florida keys 1969 camille slammed ashore mississippi louisiana 
1980 allen mightiest caribbean storm recorded lost much punch hit gulf coast texas 
future like past pattern atmospheric conditions good probability return stronger storms gray said 
1990s warns u.s. destruction least four five times costly '50s '60s boom population property development along coastal areas 
threat global warming also portends hurricanes powerful yet recorded says meteorologist kerry emanuel massachusetts institute technology 
hurricanes like huge self-sustaining heat engines spinning across sea 
get power water warmth 
develop need tropical ocean-surface temperatures 80 degrees fahrenheit 
tropical ocean temperatures go intensity hurricanes emanuel explains 
sea-surface temperatures set upper limits 
biggest uncertainty says whether global warming affect tropical ocean temperatures 
gradual warming earth results greenhouse effect caused primarily accumulation carbon dioxide methane atmosphere like glass greenhouse trap heat 
hurricanes part romance tropics 
great storms madefictional plots b movies unmade real dreams people would implant civilization environment could swallowed nature moment 
hurricane ended dream railroad florida keys 1935 virtually isolated key west two years 
nine years earlier big one stopped miami first real-estate boom tracks pushing floodwaters inland new jai alai fronton near miami international airport sits today 
newspaper headlines documented miami subsequent hurricanes 
city lashed '47 slammed '49 pounded '50 
last hit miami cleo small one 1964 intervening period calm defies odds 
many folks worry big one californians worry big quake 
would one legendary storms past 200 mph winds pushing huge tidal surges low-lying metropolis atlantic one sure 
hurricane season officially extends june 1 nov. 30 scientists national hurricane center tucked away sixth floor nondescript office building miami suburb perking another winter lull 
end season six hurricane specialists monitor weather computers 24 hours day 
part world technology made obsolete disasters unnamed hurricane surprised galveston texas 1900 killing 6,000 people 
hurricane specialists use nation working geostationary weather satellite follow developing storms 
disturbance becomes threatening specially equipped plane sent storm heart measure barometric pressure wind speed temperature 
one specialists lixion avila begun counting tropical waves form coast africa every days hurricane season 
60 waves air water form every year 
20 intensify stronger disturbances ranging tropical depressions wind speeds 38 mph tropical storms winds 39 73 mph hurricanes winds exceeding 74 mph 
last year 16 tropical depressions developed atlantic 
fourteen became tropical storms seven became hurricanes fewer usual 
people venture dubious endeavors long-term hurricane forecasting said could another light year 
said relatively quiet year guarantee thing 
one year 1935 six hurricanes formed one strongest hit continent 
800 workers brought build overseas highway florida keys killed 200 mph winds knocked evacuation train tracks 
matter long-range forecasters say scientists hurricane center know laws probability closing miami 
conservative bunch given sunday-supplement scare stories 
miami grown immensely since last major storm greatly overdue said 
historically south florida hurricanes site said jerry jarrell hurricane specialist eyeing miami 27-year hiatus nervously 
would expect major hurricane hit within 75 miles miami every eight years 
center scientists built computer model calculate would happen 1926 hurricane hit miami today 
storm almost exactly strength hurricane hugo hit south carolina 135 mph winds september 1989 
scientists took data hugo applied path taken 1926 hurricane miami 
would totally different story said max mayfield collected photos 1926 storm illustrate model 
many high-rises barrier islands south carolina none 
think folks miami beach condos going rude awakening 
worst-case storm winds topping 150 mph would put 5 feet water joe stone crabs restaurant miami beach landmark 9 feet water biscayne boulevard downtown miami surging water could reach levels 13 1/2 feet low-lying areas south miami 
despite modern technology specialists forecast precisely hurricane hit 
hurricanes change direction intensify matter hours 24 hours specialists said could miscalculate one track 100 miles 
number years since last major storm hit spawned certain complacency among residents specialists find disturbing 
mayfield went miami beach recently discuss model officials heard worried 
fire department survey 90 percent people talked said would evacuate hurricane mayfield said 
makes hair stand straight 
hurricane activity u.s. east coast intensify warned weather researcher reported new method predicting atlantic tropical storms 
william gray professor atmospheric science colorado state university hurricane forecasting specialist said uncovered strong association seasonal rainfall west africa formation intense hurricanes 
based new findings mr. gray predicted earlier summer 1990 tropical storm season would average far seems right 
july said would 11 storms severe enough named six would hurricanes 
national hurricane center said yesterday nine named storms year including four hurricanes higher rate normal 
weather researchers seeking predict hurricane patterns accurately early warning coastal residents u.s. caribbean 
sharp decrease hurricane activity past two decades spurred business residential development would vulnerable increased hurricane activity 
mr. gray said west african sahel region newest five indicators uses predict global hurricane activity 
atlantic hurricanes form especially strong tropical storms triggered winds air disturbances sweep across africa 
wind moving across warm ocean air causes powerful updrafts air bearing sea water eventually turn rainclouds 
mr. gray said believes absence moisture storms quickly dissipate storms gather strength additional moisture 
mr. gray said research published current issue science suggests intense caribbean hurricanes gilbert joan hugo 1988 1989 may forerunners change 
comparing year-to-year rainfall levels sahel number high-powered atlantic hurricanes mr. gray found striking correlation 
1947 1969 instance rainfall western sahel especially abundant time 13 severe hurricanes formed atlantic ocean 
1970 1988 sahel suffered terrible drought hurricane activity unusually low one severe hurricane gloria 1985 formed area 
mr. gray searched back recent history found sahel includes parts senegal mauritania mali gambia goes long periods alternating dry wet weather 
found sahel especially wet 1870 1900 1915 1935 
hurricane activity much higher time intervals sahel dry 
mr. gray predicted based historical cycles sahel entering wet period 
said drought conditions ended 1988 1989 
based 1990 storm forecast higher-than-normal rainfall sahel july 
mr. gray said research suggests climate changes sahel result global warming problems associated environmental pollution natural weather cycles 
