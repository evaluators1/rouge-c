_______________________________________________________________________________
                                  ROUGE-C
   Recall Oriented Understanding for Gisting Evaluation in Consensus Speaker
             Jonathan Rojas Simón - ids_jonathan_rojas@hotmail.com
_______________________________________________________________________________

Este repositorio guarda los avances realizados con respecto al método de 
evaluación de ROUGE-C, el cual es una derivación de ROUGE-1.5.5 del trabajo de
(Lin, 2004), el cual cuenta con la diferencia de realizar el proceso de
evaluación sin el uso de resúmenes generados por los humanos (resúmenes gold-
standard). En lugar de realizar la evaluación de resúmenes candidato con los 
resúmenes gold-standard, se realizan con los documentos originales (docs. 
fuente), los cuales cuentan con la información necesaria para realizar un 
proceso de evaluación. 

El uso del ROUGE sin utilizar resúmenes de referencia surge del trabajo de 
(He et al., 2008), donde plantean la hipótesis de que a la cantidad de 
imformación necesaria para realizar la evaluación es suficiente sin utilizar
resúmenes generados por los humanos. 
Por otro lado, a partir de la métodología de desarrollo de software basada por
componentes, se plantea usar esta evaluación para procesos de reconocimiento 
de patrones a partir de la determinación de la similitud (o disimilitud) de 
objetos o entidades con características definidas.

-------------------------------------------------------------------------------

SINTAXIS

java -jar ROUGE-C-1.0.jar -CONFIG CONFIG.txt


El archivo CONFIG.txt especifica la siguiente sintaxis:

    -INDIR:str -EXT:str -TODIR:str -EXTTD:str -READAS:EVAL:VS -PARAMS:str 
    -OUT:str [-MODE:SIM:DIS]

Donde:

-INDIR    Es el directorio donde se encuentran todos los resúmenes de entrada.
          Para que todos los resúmenes sean evaluados, es necesario que los 
          mismos se encuentren formateados en una sola extensión (*.txt, *.sum
          , etc.).
          NOTA: Se sugiere que cada linea del archivo de entrada se enuentre 
          una sola oración por separado, ya que el evaluador ROUGE-C trabaja 
          mejor con esta especificación.

-EXT      Indica la extensión de los resúmenes de entrada. Las extensiones
          deben de comenzar con el caracter punto (.) para que sean válidas y
          reconocibles por este componente, (Por ejemplo .txt, .sum, .pir etc).

-TODIR    Es el directorio donde se almacenan los textos de entrada (textos de
          referencia), los cuales son los documentos originales en cuestión. Al
          igual que -INDIR, es necesario que todos los textos de referencia se
          encuentren en una sola extensión (*.txt, *.sum, etc.).

-EXTTD    Indica la extensión de los textos originales. Las extensiones deben 
          de comenzar con el caracter (.) para que sean válidas y reconocibles
          por este componente, (Por ejemplo .txt, .sum, .pir, etc).

-READAS   Determina el modo de operación del componente. Las opciones 
          disponibles son las siguientes:
            - EVAL -> Evaluación de resúmenes
            - VS   -> Comparación de documentos entre sí
          Con la opción EVAL se realiza el proceso de evaluación de resúmenes 
          del directorio -INDIR en comparación con los documentos originales 
          establecidos en -TODIR. 
          En caso de que se establezca la opción VS, entonces se hará una
          comparación entre objetos a partir de los documentos de -INDIR y los
          documentos -TODIR.
          NOTA: Para esta versión se sugiere utilizar solo la opción "EVAL",
          debido a que la opción "VS" presenta algunos errores en su operación.

-PARAMS   Ruta y nombre del archivo de texto donde se encuentran especificados
          varios parámetros con los que trabaja el sistema de evaluación 
          ROUGE-1.5.5. La lista de parámetros que ocupa ROUGE-1.5.5 son los
          siguientes:

          -ROUGE:str -REH:str -NGRAM:int -STEM:bool -MGL:int 
          -UNSKB:bool -RSKB:bool -LCS:bool -CONINT:int -NSAMPL:int 
          -SCOREF:str -ALPHA:float -TOKENS:int [-OTHER:str] -SYSTEM:int

          La descripciones que acompaña los anteriores parámetros son los 
          siguientes:

          -ROUGE    Ruta y nombre del archivo que contiene todos los scripts
                    necesarios para la ejecución del sistema ROUGE. Se sugiere
                    que por defecto el nombre del archivo sea ROUGE-1.5.5.pl,
                    sin embargo, se puede ocupar cualquier otro nombre 
                    (siempre y cuando tenga una extensión .pl).

          -REH      Ruta y nombre del directorio que guarda varios archivos
                    complementarios de WordNet 2.0 para la realización de 
                    eliminación de palabras vacías (stopwords). Además, se
                    sugiere que también el nombre de este directorio sea "data"
                    por defecto.

          -NGRAM    Determina el uso de diferentes métodos de ROUGE-N para
                    evaluar (para conocer más detalles de ROUGE-N consultar 
                    (Lin, 2004)). Por ejemplo, si se especifica que -NGRAM sea
                    de 4, entonces el sistema realizará la evaluación de cuatro
                    métodos de evaluación (derivados de ROUGE-N), es decir, 
                    realizará la evaluación a través de ROUGE-1, ROUGE-2, 
                    ROUGE-3 y ROUGE-4.

          -STEM     Determina el uso de stemming de Porter (Porter, 1980) en
                    la evaluación de cualquier método de evaluación. Si se
                    desea el uso de stemming, entonces -STEM deberá tener
                    el argumento "true", en caso contrario se deberá 
                    especificar "false".

          -MGL      (Skip unigrams) Parámetro que determina el uso de 
                    bolsa de palabras con salto de N. Donde el argumento de
                    -MGL determina el salto de palabras para realizar la
                    recuperación y coincidencia de palabras.

          -UNSKB    Determina el uso del modelo de texto de bolsa de palabras
                    en los bigramas con saltos (skip bigrams). Este parámetro 
                    es útil cuando se trabaja con el método de ROUGE-SU4. Si 
                    se desea incluir esta característica en la evaluación de
                    ROUGE-SU4, colocar como argumento "true", de lo contrario
                    se deberá colocar "false".

          -RSKB     Este parámetro es similar a -UNSKB, pero además de trabajar
                    con bolsa de palabras trabaja con bigramas con saltos 
                    regulares, en caso de que se le coloque el argumento "true"
                    . En caso contrario se colocará "false".

          -LCS      Determina el uso de subsecuencias más largas de cadenas 
                    (Longest Common Subsequences) como otro método de evalua-
                    ción (ROUGE-LCS). En caso de utilizarlo, colocar el valor
                    de "true", en caso contrario colocar "false".

          -CONINT   Especifica el intervalo de confianza con el que trabajan
                    todos los evaluadores. Por defecto se coloca con 95.

          -NSAMPL   Número de muestras que se tomarán como límite en la 
                    evaluación. Normalmente se trabaja con el valor de 1000.

          -SCOREF   Determina el uso de fórmulas de puntajes. Para este 
                    parámetro se manejan solo con las opciones de "A" o "B".
                    Para mayor detalle sobre el uso de estos argumentos,
                    consultar (Lin, 2004).

          -ALPHA    Determina la importancia del recuerdo y la precisión de
                    los puntajes de ROUGE en un rango de 0-1, donde 0 se le
                    da más importancia al recuerdo y 1 favorece a la precisión.

          -TOKENS   Realiza el cálculo promedio de ROUGE por medio del
                    promediado de todo el corpus de prueba en lugar de 
                    oraciones (unidades).
                    Caso 0: Usar oraciones como unidades de conteo.
                    Caso 1: Usar cada token como unidad de conteo.
                    Caso 2: Similar al caso 0, pero con conteos sin procesar.
                            En lugar de realizar precisión, recuerdo y 
                            F-measure antes, el caso 2 es útil cuando se 
                            desean calcular las tres métricas mencionadas 
                            anteriormente al final.

          -OTHER    Determina el uso de otros parámetros no vistos por esta 
                    versión del evaluador. Para que se utilicen se deben 
                    colocar como se especificarían en la evaluación de ROUGE.

          -SYSTEM   Determina el uso del ID de evaluación de resúmenes. Por 
                    defecto colocar el valor de 1.



-OUT      En caso de que se marque la opción EVAL, en esta etiqueta se debe 
          especificar el directorio y nombre de archivo que guarde los 
          resultados de evaluación de los resúmenes de -INDIR con los 
          documentos originales de -TODIR.
          En caso de que se marque la opción VS, en esta etiqueta se debe 
          especificar el directorio de salida donde se guardará la comparación
          de cada archivo en -INDIR con cada archivo en -TODIR.

-MODE     Determina el tipo de cálculo a realizar, en caso de que se quiera 
          calcular la similitud especificar SIM, en caso contrario (distancia).
          se deberá especificar DIS. Cualquier opción que no pertenezca a estas
          dos opciones se calculará como distancia. Si se pretende realizar el
          proeso de evaluación, se sufiere utilizar el argumento SIM.

Referencias
-------------------------------------------------------------------------------
(He et al., 2008) - He, T., Chen, J., Ma, L., Gui, Z., Li, F., Shao, W., & 
                    Wang, Q. (2008, August). ROUGE-C: A fully automated 
                    evaluation method for multi-document summarization. In 
                    Granular Computing, 2008. GrC 2008. IEEE International 
                    Conference on (pp. 269-274). IEEE.

(Lin, 2004)       - Lin, C. Y. (2004). Rouge: A package for automatic
                    evaluation of summaries. Text Summarization Branches Out.